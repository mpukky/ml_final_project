import numpy as np  # linear algebra
import pandas as pd  # data processing, CSV file I/O (e.g. pd.read_csv)
import seaborn as sns  # data visualization library
import matplotlib.pyplot as plt
from pandas import DataFrame, ExcelWriter
import numpy.linalg as LA
from sklearn.ensemble import RandomForestClassifier
from sklearn.cross_validation import train_test_split

###import data.cvs
# print(check_output(["ls", "../Downloads/"]).decode("utf8"))
from sklearn.metrics import confusion_matrix, accuracy_score

data = pd.read_csv('../Downloads/data.csv')
#print data.head()  # head method show only first 5 rows

###Get rid of id, Diagnosis, and Unnamed:32 colunms, so we have 30 usable features left
# feature names as a list
col = data.columns  # .columns gives columns names in data
# print(col)
# y includes our labels and x includes our features
y = data.diagnosis  # M or B
list = ['Unnamed: 32', 'id', 'diagnosis']
x = data.drop(list, axis=1);  # print x.head()

###Find total no. of Benign and Malignat provided in data set
# ax = sns.countplot(y,label="Count")       # M = 212, B = 357
B, M = y.value_counts()
print('Number of Benign: ', B)
print('Number of Malignant : ', M)
###find count,mean,std,min,max of each feature
# print x.describe()
xNumpy = DataFrame.as_matrix(x);  # print type(xNumpy); #use this for finding prob of whole training data set

# split data train 70 % and test 30 %
x_train, x_test, y_train, y_test = train_test_split(x, y, test_size=0.3, random_state=42)
# normalization
x_train_N = (x_train - x_train.mean()) / (x_train.max() - x_train.min())
x_test_N = (x_test - x_test.mean()) / (x_test.max() - x_test.min())
x_trainNp = DataFrame.as_matrix(x_train); #print len(x_trainNp)
x_testNp = DataFrame.as_matrix(x_test); #print len(x_testNp)
B1, M1 = y_train.value_counts()
print('Number of Benign 70% train: ', B1)
print('Number of Malignant 70% train: ', M1)
B2, M2 = y_test.value_counts()
print('Number of Benign 30% test: ', B2)
print('Number of Malignant 30% test: ', M2)

#################################

###Find Z
mu = np.mean(x_trainNp, axis=0);  # print 'mean', (mu)

# print 'mean array type', type(mu); print 'mean size',(mu.shape); #print "mean min/max values:",(np.amin(mu),np.amax(mu))
Z = x_trainNp - mu;  # print 'Z', (Z)
# print Z.shape  #12873*784 same as X
# print 'Z mean', np.mean(Z,axis=0)   #must be zero
# print "Z min/max values:",(np.amin(Z),np.amax(Z))

###Find C
C = np.cov(Z, rowvar=False);  # print 'C', (C)
# print np.allclose(C.T, C)   #check C and C transpose are symmetry; C = C.T, this should return True

###Find V
[sumEigens, V] = LA.eigh(C);  # print 'sumAllEigenValues', (sumEigens,'\n\n'); #python gives result as row to col and least importance as 1st, so need to transpose and  flip
# print 'V', V
row = V[0, :];
col = V[:, 0];
sumEigens = np.flipud(sumEigens);
V = np.flipud(V.T);  # flip column upside down of V transpose
row = V[0, :];  # Check once again
#print np.dot(C, row) - (sumEigens[0] * row)  # should be zero
#print np.allclose(np.dot(C, row), sumEigens[0] * row)  # should say True
# print LA.norm(V[1])  #should = 1
# print np.dot(V[0,:],V[1,:]) #should =~ 0 #orthogonality or dot product of 2 eigenvectors must be 0 or near 0


EigenVectorsTwoCols = V[0:2, :]  # manipulate to get only first 2 rows to be eigenvectors
# print EigenVectorsTwoCols.shape #size 2*784
###Find M(malignant) and B(benigh)

###Find P
P = np.dot(Z, EigenVectorsTwoCols.T);  # print 'P', (P) #Principal components
#print 'P mean', np.mean(P, axis=0)  # must be zero or close to zero
###Find R
Xrec1 = (np.dot(P[:, 0:1],
                V[0:1, :])) + mu;  # print 'Xrec1', (Xrec1) ;#Reconstruction using 1 component, P col#1 * V row#1 + mu
Xrec2 = (np.dot(P[:, 0:2], V[0:2, :])) + mu;  # print 'Xrec2', (Xrec2) ;#Reconstruction using 2 components

###Build 2D Histograms
Bin1=int(np.log2(M1)+1)+1
Bin2=int(np.log2(B1)+1)+1
#print Bin1, Bin2
L = pd.Series(y_train).values; #print L,len(L)
Bin = Bin1
p1 = P[:, 0]
p2 = P[:, 1]
p1min = min(p1); #print 'p1 min', p1min
p1max = max(p1); #print 'p1 max', p1max
p2min = min(p2); #print 'p2 min', p2min
p2max = max(p2); #print 'p2 max', p2max

def Build2DHistogramClassifier(p1, p2, L, Bin, p1max, p1min, p2max, p2min):
    H1 = np.zeros(shape=(Bin, Bin), dtype=np.int64);
    H2 = np.zeros(shape=(Bin, Bin), dtype=np.int64);
    r = (np.round(((Bin - 1) * (p1 - p1min) / (p1max - p1min)))).astype('int32');
    c = (np.round(((Bin - 1) * (p2 - p2min) / (p2max - p2min)))).astype('int32');
    zipped = zip(r, c)
    # print list(zipped)
    for i, (a, b) in enumerate(zipped):
        if L[i] == 'M':
            H1[a, b] += 1;
        else:
            H2[a, b] += 1;
    return [H1, H2]


[H1, H2] = Build2DHistogramClassifier(p1, p2, L, Bin, p1max, p1min, p2max, p2min);  # H1 for Maglinant, H2 for Benigh
print H1, H1.sum()
print H2, H2.sum()


###Apply 2DHistogram to find Digit result and Histogram Prob
def Apply2DHistogramClassifier(queries, H1, H2, p1min, p1max, p2min, p2max):
    B = np.alen(H1);
    r = (np.round(((B - 1) * (queries[0] - p1min) / (p1max - p1min)))).astype('int32');
    c = (np.round(((B - 1) * (queries[1] - p2min) / (p2max - p2min)))).astype('int32');
    countp = H1[r, c];
    countn = H2[r, c];
    #print countp, countn
    resultlabel = np.full(np.alen(r), "Indetermeinate", dtype=object);
    indicesp = countp > countn;
    indicesn = countn > countp;
    resultlabel[indicesp] = 'M';
    resultlabel[indicesn] = 'B';
    if countp == countn == 0:
        resultprob = 'NA'
        #print 'undecidable'
    elif resultlabel == 'M':
        resultprob = float(countp) / float(countp + countn);
    elif resultlabel == 'B':
        resultprob = float(countn) / float(countp + countn);
    #print resultlabel, resultprob
    return resultlabel, resultprob


###calculate Training accuracy attained using Histograms and all columns of features
dLabelp = [];
hProbp = []
count = 0
#print x_testNp.shape, EigenVectorsTwoCols.T.shape
for i in range(0, len(x_testNp)):
    z = x_testNp[i] - mu
    p = np.dot(z, EigenVectorsTwoCols.T)
    [A, C] = Apply2DHistogramClassifier(p, H1, H2, p1min, p1max, p2min, p2max)
    dLabelp.append(A);
    hProbp.append(C);
    #print dLabelp[i], hProbp[i]
    if hProbp[i] > 0.5 and hProbp[i] != 'NA':
        count += 1
print count, len(x_testNp)
trainingAccuracyH = float(count) / float(len(x_testNp)) * 100
print 'trainingAccuracyHistogram', trainingAccuracyH

###Find mean mup, mun, cp, cn for calculating Bayesian classifier
Pp = [];
Pn = []
Lp = [];
Ln = []
for i in range(len(y_train)):
    if L[i] == 'M':
        Pp.append(P[i])
        Lp.append(L[i])
    else:
        Pn.append(P[i])
        Ln.append(L[i])
Pp = np.array(Pp);
Pn = np.array(Pn)
mup = np.mean(Pp, axis=0);
mun = np.mean(Pn, axis=0);
#print 'mup', mup; print 'mun', mun
###Find covariance cp and cn
cp = np.cov(Pp, rowvar=False);  # print 'cp', (cp)
cn = np.cov(Pn, rowvar=False);  # print 'cn', (cn)


###Find Bayesian Classifier
def calculateBayesian(p, mu, cov, N):
    covDet = np.linalg.det(cov)
    # print covDet
    covInv = np.linalg.inv(cov)  # [2 X 2]
    # print covInv
    pMinusMu = np.asmatrix(p - mu)  # [1 X 2]
    # print pMinusMu
    pMinusMuT = pMinusMu.reshape(2, 1)  # [2 X 1]
    # print pMinusMuT
    Q = float(N) / np.math.sqrt(covDet) * np.math.exp(-0.5 * pMinusMu * covInv * pMinusMuT)
    return Q


###calculate Training accuracy attained using Bayesian
count1 = 0;
count2 = 0
L2 = pd.Series(y_test).values; #print L,len(L)
for i in range(0, len(x_testNp)):
    z = x_testNp[i] - mu
    p = np.dot(z, EigenVectorsTwoCols.T)
    Q1 = calculateBayesian(p, mup, cp, M2)
    Q2 = calculateBayesian(p, mun, cn, B2)
    if Q1 + Q2 != 0:
        pBF1 = Q1 / (Q1 + Q2);
        pBF2 = Q2 / (Q1 + Q2)
        #print i, L[i], pBF1, pBF2
    else:
        print 'Q1+Q2 = 0'
    if (pBF1 > pBF2) and (L2[i] == 'M'):
        count1 += 1
        #print i, L[i], pBF1, pBF2
    if (pBF2 > pBF1) and (L2[i] == 'B'):
        count2 += 1
        #print i, L[i], pBF1, pBF2
print count1, count2, count1 + count2
trainingAccuracyBF = float(count1 + count2) / float(len(x_testNp)) * 100
print 'trainingAccuracyBF', trainingAccuracyBF

"""
###Draw Scatter Plot
XB = np.linspace(-1,1,18)
YB = np.linspace(-1,1,18)
X,Y = np.meshgrid(XB,YB)
Z = H1
plt.imshow(Z,interpolation='none')

XB = np.linspace(-1,1,18)
YB = np.linspace(-1,1,18)
X,Y = np.meshgrid(XB,YB)
Z2 = H2
plt.imshow(Z2,interpolation='none')

labeln=1
labelp=2
#T = np.ndarray.tolist(T); #print T; #print type(P)
#Pn = np.random.multivariate_normal(mun, cn, Nn)
Ln = np.full((M,), labeln, dtype=np.int)
Lp = np.full((B,), labelp, dtype=np.int)
P = np.concatenate((Pn,Pp));
L = np.concatenate((Ln,Lp));

# For best effect, points should not be drawn in sequence but in random order
np.random.seed(0)
randomorder=np.random.permutation(np.arange(len(L)));
randomorder=np.arange(len(L));
# Set colors
cols=np.zeros((len(L),4))    # Initialize matrix to hold colors [12873X4]
cols[L==labeln]=[1,0,0,1] # Negative points are red (with opacity 0.25)
cols[L==labelp]=[0,1,0,1] # Positive points are green (with opacity 0.25)
# Draw scatter plot
fig = plt.figure()
ax = fig.add_subplot(111, facecolor='black')
ax.scatter(P[randomorder,1],P[randomorder,0],s=5,linewidths=0,facecolors=cols[randomorder,:],marker="o");
ax.set_aspect('equal')
plt.gca().invert_yaxis()
plt.show()
"""

###Find classifier linear classes
#Find W
Xa=np.insert(x_trainNp, 0, values=1, axis=1); #print Xa; [6600x16]; # insert colunm value 1 before column 0th
XaInv=np.linalg.pinv(Xa,rcond=1e-15); #print XaInv.shape; #[16X6600]
label = pd.Series(y_train).values;
T = []
for i in range(0, len(label)):
    if label[i] == 'M':
        T.append(1)
    else:
        T.append(-1)
W1=np.dot(XaInv,T); #print W1
#Find new T
Xa2=np.insert(x_testNp, 0, values=1, axis=1); #print Xa2; #[50x16]; # insert colunm value 1 before column 0th
Xa2W1=np.dot(Xa2,W1)
NewT2=np.sign(Xa2W1)
#sort all old T
label2 = pd.Series(y_test).values;
T2 = []
for i in range(0, len(label2)):
    if label2[i] == 'M':
        T2.append(1)
    else:
        T2.append(-1)

#compare given and new found T
PPcount=0; FPcount=0; PNcount=0; FNcount=0
for i in range(0,len(label2)):
    if (T2[i] == 1) and (NewT2[i] == 1):
        PPcount += 1
    if (T2[i] == 1) and (NewT2[i] == -1):
        PNcount += 1
    if (T2[i] == -1) and (NewT2[i] == -1):
        FPcount += 1
    if (T2[i] == -1) and (NewT2[i] == 1):
        FNcount += 1
print  PPcount, FPcount, PNcount, FNcount, PPcount+PNcount+FPcount+FNcount

bAccuracy=float(PPcount+FPcount)/float(PPcount+PNcount+FPcount+FNcount); print bAccuracy
bSensitivity=float(PPcount)/float(PPcount+PNcount); print bSensitivity
bSpecificity=float(FPcount)/float(FPcount+FNcount); print bSpecificity
bPPV=float(PPcount)/float(PPcount+FNcount); print bPPV


cm_2 = confusion_matrix(T2,NewT2)
sns.heatmap(cm_2,annot=True,fmt="d")
plt.show()

def BuildLinearClassifier(Xa, W1):
    label = pd.Series(y_train).values;
    T = []
    for i in range(0, len(label)):
        if label[i] == 'M':
            T.append(1)
        else:
            T.append(-1)
    # print T

    ###Find Performance
    XaW1 = np.dot(Xa, W1)
    NewFail = np.sign(XaW1);  # print NewFail; #[6600x1]

    # compare given and new found Fail (XaW1)
    PPcount = 0;
    FPcount = 0;
    PNcount = 0;
    FNcount = 0
    for i in range(0, len(T)):
        if (T[i] == 1) and (NewFail[i] == 1):
            PPcount += 1
        if (T[i] == 1) and (NewFail[i] == -1):
            PNcount += 1
        if (T[i] == -1) and (NewFail[i] == -1):
            FPcount += 1
        if (T[i] == -1) and (NewFail[i] == 1):
            FNcount += 1
    # print  PPcount, FPcount, PNcount, FNcount, PPcount+PNcount+FPcount+FNcount

    bAccuracy = float(PPcount + FPcount) / float(PPcount + PNcount + FPcount + FNcount);
    print 'accuracy', bAccuracy*100
    # bSensitivity=float(PPcount)/float(PPcount+PNcount); print 'sensitivity', bSensitivity
    # bSpecificity=float(FPcount)/float(FPcount+FNcount); print 'specificity', bSpecificity
    # bPPV=float(PPcount)/float(PPcount+FNcount); print 'PPV', bPPV


for i in range(1, 31):
    print 'linear classifier for', i, 'components'
    EigenVectorsTwoCols = V[0:i, :]
    ###Find P
    P = np.dot(Z, EigenVectorsTwoCols.T);  # print 'P', (P) #Principal components
    Xrec = (np.dot(P[:, 0:i], V[0:i, :])) + mu;
    Xa = np.insert(Xrec, 0, values=1, axis=1)
    BuildLinearClassifier(Xa, W1)

from sklearn.feature_selection import SelectKBest
from sklearn.feature_selection import chi2
# find best scored 5 features
select_feature = SelectKBest(chi2, k=4).fit(x_train, y_train)
print('Score list:', select_feature.scores_)
print('Feature list:', x_train.columns)

x_train_2 = select_feature.transform(x_train)
x_test_2 = select_feature.transform(x_test)
#random forest classifier with n_estimators=10 (default)
clf_rf_2 = RandomForestClassifier()
clr_rf_2 = clf_rf_2.fit(x_train_2,y_train)
ac_2 = accuracy_score(y_test,clf_rf_2.predict(x_test_2))
print('Accuracy is: ',ac_2)